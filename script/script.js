const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

function isValidBook(book) {
  return book.hasOwnProperty('author') && book.hasOwnProperty('name') && book.hasOwnProperty('price');
}

const rootDiv = document.getElementById('root');
const ul = document.createElement('ul');

books.forEach(book => {
  try {
    if (isValidBook(book)) {
      const li = document.createElement('li');
      li.textContent = `${book.author ? book.author + ': ' : ''}${book.name} - ${book.price}$`;
      ul.appendChild(li);
    } else {
      throw new Error('Помилка: недостатня кількість властивостей');
    }
  } catch (error) {
    console.error(error.message);
  }
});

rootDiv.appendChild(ul);
